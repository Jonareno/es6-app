import { BaseElement } from './base-element.js';

export class HighChart extends BaseElement {

    constructor(containerId, data) {
        super();
        this.containerId = containerId;
        this.data = data;
    }

    createElement() {
        super.createElement();

        setTimeout(() => {
            //this is a depedancy code smell. There's no explicit requirement for Highcharts. 
            Highcharts.chart(this.containerId, {

                title: {
                    text: 'Last 30 days'
                },

                yAxis: [{
                    title: {
                        text: 'Price'
                    }
                },{
                    title: 'Volume',
                    opposite: true
                }],
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                series: [{
                    name: 'Open',
                    data: (function(data){
                        var open = [];
                        for (var prop in data['Time Series (Daily)']) 
                            open.push(+data['Time Series (Daily)'][prop]["1. open"] );
                        return open;
                    })(this.data)
                }, {
                        name: 'High',
                        data: (function (data) {
                            var open = [];
                            for (var prop in data['Time Series (Daily)'])
                                open.push(+data['Time Series (Daily)'][prop]["2. high"]);
                            return open;
                        })(this.data)
                    }, {
                        name: 'Low',
                        data: (function (data) {
                            var open = [];
                            for (var prop in data['Time Series (Daily)'])
                                open.push(+data['Time Series (Daily)'][prop]["3. low"]);
                            return open;
                        })(this.data)
                    }, {
                        name: 'Close',
                        data: (function (data) {
                            var open = [];
                            for (var prop in data['Time Series (Daily)'])
                                open.push(+data['Time Series (Daily)'][prop]["4. close"]);
                            return open;
                        })(this.data)
                    }, {
                        name: 'Volume',
                        yAxis: 1,
                        data: (function (data) {
                            var open = [];
                            for (var prop in data['Time Series (Daily)'])
                                open.push(+data['Time Series (Daily)'][prop]["5. volume"]);
                            return open;
                        })(this.data)
                    }],
/*1. open: "96.7100"
2. high: "98.7300"
3. low: "96.3200"
4. close: "98.6600"
5. volume: "21251222"*/
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }

            });
            

        }, 0);

    }

    getElementString() {
        return `<div id="${this.containerId}"></div>`;
    }

}