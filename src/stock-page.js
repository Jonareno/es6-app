import {Page} from './framework/page.js';
import {application} from './app.js';
import {HighChart} from './ui/high-chart.js';

export class StockPage extends Page {
    
    constructor(ticker) {
        super(ticker);
        this.ticker = ticker;
        this.data = null;
    }
    
    createElement() {
        super.createElement();

        let that = this;
        console.log("THIS", this);
        let stockChart;
        //this is a depedancy code smell. There's no explicit requirement for jQuery. 
        if(!this.data)
            $.get("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=MSFT&apikey=demo", function (data) { 
                that.data = data;
                stockChart = new HighChart(`${that.ticker}-chart`, data);
                stockChart.appendToElement(that.element);
                console.log("Web data: ", data);
            });
        else {
            stockChart = new HighChart(`${this.ticker}-chart`, this.data);
            stockChart.appendToElement(this.element);
            console.log("Cached Data", this.data);
        }
        
    }
    
    getElementString() {
        return `<div style="margin: 20px;"><h3>${this.ticker}</h3></div>`;
    }
}