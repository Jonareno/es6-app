import {Page} from './framework/page.js';
import {Image} from './ui/image.js';
import {Button} from './ui/button.js';
import {application} from './app.js';

export class HomePage extends Page {
    
    constructor() {
        super('Home');
    }
    
    createElement() {
        super.createElement();
       
        let styleString = 'width: 300px; height: 80px; font-size: 26px; margin: 10px;';
        let b = new Button('MSFT');
        b.setStyleString(styleString);
        b.appendToElement(this.element);
        b.element.click(() => application.activateRoute('MSFT'));
    }
    
    getElementString() {
        return '<div style="text-align: center;height:calc(100vh - 64px);background:url(\'images/wallhaven-408764.jpg\') center center;"></div>';
    }
}