import $ from 'jquery';

import {ApplicationBase} from './framework/application-base.js';
import {HomePage} from './home-page.js';
import {StockPage} from './stock-page.js';

export class App extends ApplicationBase {
    
    constructor() {
        super('Watch List');
        
        this.addRoute('Home', new HomePage(), true);
        this.addRoute('MSFT', new StockPage('MSFT'));
    }
}


// window.app = new App();
// window.app.show($('body'));

export let application = new App();
application.show($('body'));

